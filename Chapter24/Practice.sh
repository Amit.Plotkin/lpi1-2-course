#!/usr/bin/env bash
############################################
# Owner:Amit Plotkin 
# Description: Reverse String
# Version: v1.0.0.0
# Date: 20-09-2020
# Note: 
#1. Write a script that uses a for loop to count from 3 to 7.
#2. Write a script that uses a for loop to count from 1 to 17000.
#3. Write a script that uses a while loop to count from 3 to 7.
#4. Write a script that uses an until loop to count down from 8 to 4.
#5. Write a script that counts the number of files ending in .txt in the current directory.
#6. Wrap an if statement around the script so it is also correct when there are zero files ending in .txt
############################################

#set -x

echo "start Execute"

#1
echo "thi is task 1 count from 3 to 7"
for cntr in {3..7}
do 
    echo $cntr
done

#2
echo "thi is task 1 count from 3 to 7"
for cntr in {1..17000}
do 
    echo $cntr
    if [[ $cntr == 10 ]];then
        break;
    fi
    
done


#3
let i=3
while [ $i -le 7 ]
do
    echo $i;
    ((i++))
done

#4
index=8
until [[ $index < 4 ]]
do
    echo $index;
    ((index--))
done

#5 this is not working
fileCounter=0
for _file in *.txt
do
echo $_file
    ((fileCounter++))
done
echo "there are $fileCounter files in the Directory"


echo "start MyLoop"
 
 dirlist=(`ls`) #read files into variable dirList
echo "************************************"
#loop dirlist items
for f in ${dirlist[*]} 
do
    echo "file name: " $f
done
echo "***" $dirlist

echo "1)" ${#dirlist[*]} #return number of files
echo "2)" ${!dirlist[*]} #return keys of the files (indexes)
echo "3)" ${dirlist} # returns only the first index
echo "4)" ${!dirlist} #error this is wrong
echo "5)" ${dirlist[@]}  # return the list of files
echo "6)" ${dirlist[*]}  # return the list of files

echo "5)" ${!dirlist[@]}  #
echo "6)" ${!dirlist[*]}  #

echo "5)" ${#dirlist[@]}  #
echo "6)" ${#dirlist[*]}  #



echo "Done"


####################################################class examples#################################

# basic loop
for iterator in 0 1 2 3 4 5 6 7 8
do
    echo $iterator
done

for iterator in `0 1 2 3 4 5 6 7 8`
do
    echo $iterator
done

for iterator in {0..10}
do
    echo $iterator
done
# this is the way you execute functions that you should loop over the result array
echo "loop over function"
# for i in `seq 0 7` # this is the different way to execute function use back tick ( `` )
for i in $(seq 0 7)
do
    echo $i;
done

echo "loop over function"
for i in $(ls *.txt)
do
    echo $i;
done

# execute the ls comand and throw the result into "trash" into null folder and return exit status
exis_status=$(ls *.txt &> /dev/null;echo $?);
echo $exis_status
if [[ $exis_status -eq 0 ]];
then
cnt=0;
    for _file in *.txt
    do
        echo $_file;
        ((cnt++))
    done

    echo "counter is : " $cnt
else
    echo "HAHAHA"
fi



###################################while loop#########################
