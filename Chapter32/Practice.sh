#!/usr/bin/env bash
############################################
# Owner:Amit Plotkin 
# Description: 
# Version: v1.0.0.0
# Date:
# Note: 
############################################

#1. As normal user, create a directory ~/permissions. Create a file owned by yourself in there.
    mkdir ~/permissions
    touch  ~/permissions/MyTestFile.txt
#2. Copy a file owned by root from /etc/ to your permissions dir, who owns this file now ?
     cp /etc/passwd ~/permissions
#3. As root, create a file in the users ~/permissions directory.
    #[root@student permissions]# touch RootFile.txt
#4. As normal user, look at who owns this file created by root.
    [student@student permissions]$ ls -la
#total 8
#drwxrwxr-x.  2 student student   62 Oct 29 12:13 .
#drwx------. 13 student student 4096 Oct 29 12:09 ..
#-rw-rw-r--.  1 student student    0 Oct 29 12:12 MyTestFile.txt
#-rw-r--r--.  1 student student 2896 Oct 29 12:12 passwd
#-rw-r--r--.  1 root    root       0 Oct 29 12:13 RootFile.txt

#5. Change the ownership of all files in ~/permissions to yourself.
chown  student  /home/student/permissions/*
#6. Make sure you have all rights to these files, and others can only read.
chmod u=rwx,g=r,o=r RootFile.txt 
#7. With chmod, is 770 the same as rwxrwx--- ?
#yes
#8. With chmod, is 664 the same as r-xr-xr-- ?
 chmod 664 RootFile.txt 
#[student@student permissions]$ ls -la RootFile.txt 
#-rw-rw-r--. 1 student root 0 Oct 29 12:13 RootFile.txt
#NO
#9. With chmod, is 400 the same as r-------- ?
chmod 400 RootFile.txt 
#[student@student permissions]$ ls -la RootFile.txt 
#-r--------. 1 student root 0 Oct 29 12:13 RootFile.txt
#YES
#10. With chmod, is 734 the same as rwxr-xr-- ?
chmod 734 RootFile.txt 
# ls -la RootFile.txt 
#-rwx-wxr--. 1 student root 0 Oct 29 12:13 RootFile.txt
#No

#11a. Display the umask in octal and in symbolic form.
umask
#11b. Set the umask to 077, but use the symbolic format to set it. Verify that this works.
 umask 077
#12. Create a file as root, give only read to others. Can a normal user read this file ? Test writing to this file with vi.
 echo "hello " > RootFile123.txt
 #~                                                                                                                                                                                                                                            
#-- INSERT -- W10: Warning: Changing a readonly file    

#13a. Create a file as normal user, give only read to others. Can another normal user read this file ? Test writing to this file with vi.
 #others can read
#13b. Can root read this file ? Can root write to this file with vi ?
    #root can do anything
#14. Create a directory that belongs to a group, where every member of that group can read and write to files, and create files. Make sure that people can only delete their own files.

mkdir /home/project42 ; groupadd project42
chgrp project42 /home/project42 ; chmod 775 /home/project42