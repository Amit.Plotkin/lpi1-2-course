#!/usr/bin/env bash
############################################
# Owner:Amit Plotkin 
# Description: 
# Version: v1.0.0.0
# Date:
# Note: 
############################################
##29.10. practice: user passwords

#1. Set the password for serena to hunter2.
    sudo passwd serena
    #New password: hunter2
    #Retype new password: hunter2
    #passwd: password updated successfully
    
#2. Also set a password for venus and then lock the venus user account with usermod. Verify
#   the locking in /etc/shadow before and after you lock it.
 chage -l venus
#Last password change                                    : Oct 26, 2020
#Password expires                                        : never
#Password inactive                                       : never
#Account expires                                         : never
#Minimum number of days between password change          : 0
#Maximum number of days between password change          : 99999
#Number of days of warning before password expires       : 7
usermod -L venus

#3. Use passwd -d to disable the serena password. Verify the serena line in /etc/shadow before and after disabling.

tail /etc/shadow 
#venus:!$6$UVZD.QPaR01/i1ho$zW2.FvNdsS4kBzaQ2QFemL2hFx88GC5LKUnzVxJWkvbs70ir/WPSa67zlRremlNhyw/4fDHLR6b62fjnHVRFO1:18561:0:99999:7:::
#serena:$6$Ese1hm9sJN5g6Sv/$xlAnh71MUE3FCA7tsXAu0ZfWMQR81UUOe2FIR4HA1yzWmz5iWnJJiP3W7SXNa/XyoHgZSSeZ8oA7Jq/eVLOHi1:18561:0:99999:7:::
passwd -d serena
#Removing password for user serena.
#passwd: Success
# tail /etc/shadow 
#venus:!$6$UVZD.QPaR01/i1ho$zW2.FvNdsS4kBzaQ2QFemL2hFx88GC5LKUnzVxJWkvbs70ir/WPSa67zlRremlNhyw/4fDHLR6b62fjnHVRFO1:18561:0:99999:7:::
#serena::18561:0:99999:7:::

#4. What is the difference between locking a user account and disabling a user account's
#   password like we just did with usermod -L and passwd -d?
# -L just block the user without deleting the password (added "!" infront of the password), the operation is reversable.
# -d delete the password of the user from the shadow file

#5. Try changing the password of serena to serena as serena.
#[serena@student Desktop]$ passwd
#Changing password for user serena.
#Current password: 
#New password: 
#BAD PASSWORD: The password is shorter than 8 characters
#passwd: Authentication token manipulation error

#6. Make sure serena has to change her password in 10 days.
    chage -M 10 serena
#7. Make sure every new user needs to change their password every 10 days.
    #update vi /etc/login.defs  PASS_MAX_DAYS   10
#8. Take a backup as root of /etc/shadow. Use vi to copy an encrypted hunter2 hash from
#   venus to serena. Can serena now log on with hunter2 as a password ?

#9. Why use vipw instead of vi ? What could be the problem when using vi or vim ?
    # The vipw tool will do proper locking of the file.

#10. Use chsh to list all shells (only works on RHEL/CentOS/Fedora), and compare to cat /etc/shells.
     chsh -l
#/bin/sh
#/bin/bash
#/usr/bin/sh
#/usr/bin/bash
cat /etc/shells
#/bin/sh
#/bin/bash
#/usr/bin/sh
#/usr/bin/bash

#11. Which useradd option allows you to name a home directory ?
     #useradd -m -d "<home Dir Path>"

#12. How can you see whether the password of user serena is locked or unlocked ? Give a
#   solution with grep and a solution with passwd.
#pasword starts with !
