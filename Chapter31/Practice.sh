#!/usr/bin/env bash
############################################
# Owner:Amit Plotkin 
# Description: 
# Version: v1.0.0.0
# Date:
# Note: 
############################################

#1. Create the groups tennis, football and sports.
groupadd tennis
groupadd football
groupadd sports

#2. In one command, make venus a member of tennis and sports.
usermod -a -G tennis,sports venus
#3. Rename the football group to foot.
groupmod -n foot football
#4. Use vi to add serena to the tennis group.
    #tennis:x:1003:venus,serena
#5. Use the id command to verify that serena is a member of tennis.
id serena
    #uid=1002(serena) gid=1002(serena) groups=1002(serena),1003(tennis)
#6. Make someone responsible for managing group membership of foot and sports. Test that it works.
gpasswd -A serena sports
gpasswd -A serena foot
tail  /etc/gshadow | grep :serena
