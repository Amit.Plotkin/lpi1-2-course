#!/usr/bin/env bash
############################################
# Owner:Amit Plotkin 
# Description: Reverse String
# Version: v1.0.0.0
# Date: 15-09-2020
# Note: create script that will take a requiered disk name and will create 2 primary partitions of size 1G each.
############################################



source PartitionReposotory.sh

function main()
{
  DiskName="$@"
  Create_Partition "$DiskName"
  Create_Partition "$DiskName"
}


main "$@"