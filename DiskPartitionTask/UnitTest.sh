#!/usr/bin/env bash
set -x
############################################
# Owner:Amit Plotkin 
# Description: Reverse String
# Version: v1.0.0.0
# Date: 15-09-2020
# Note:  create script that will take a requiered disk name and will delete all current partitions partitions.
############################################

#source PartitionReposotory.sh


#result="$(Get_Disk_Partisions /dev/sda)"

#echo $result

function Create_Partition()
{
    local diskname="$1";
    local Size=+"$2"
fdisk "$diskname" <<EOF
n
p


${Size}
w
EOF

}

Create_Partition "/dev/sdb" 100M


# #this mehod delete the selected partision 
# function Delete_Partition(){
   
#     local diskname="$1";
      
#     fdisk "$diskname" << EOF
# d

# w
# EOF

# }

# Delete_Partition "/dev/sdb"