#!/usr/bin/env bash
############################################
# Owner:Amit Plotkin 
# Description: Reverse String
# Version: v1.0.0.0
# Date: 04-11-2020
# Note: 
############################################

#1. Verify whether gcc, sudo and wesnoth are installed.
 dpkg -l | grep 'gcc\|sudo\|wesnoth'
###ii  gcc                                   4:8.3.0-1                                    amd64        GNU C compiler
#ii  gcc-8                                 8.3.0-6                                      amd64        GNU C compiler
#ii  gcc-8-base:amd64                      8.3.0-6                                      amd64        GCC, the GNU Compiler Collection (base package)
#ii  gnome-sudoku                          1:3.30.0-2                                   amd64        Sudoku puzzle game for GNOME
#ii  hitori                                3.31.0-1                                     amd64        logic puzzle game similar to sudoku
#ii  libgcc-8-dev:amd64                    8.3.0-6                                      amd64        GCC support library (development files)
#ii  libgcc1:amd64                         1:8.3.0-6                                    amd64        GCC support library
#ii  linux-compiler-gcc-8-x86              4.19.152-1                                   amd64        Compiler for Linux on x86 (meta-package)
###ii  sudo                                  1.8.27-1+deb10u2                             amd64        Provide limited super user privileges to specific users

#2. Use yum or aptitude to search for and install the scp, tmux, and man-pages packages. Did you find them all ?
aptitude install scp
#  Couldn't find any package whose name is "scp", but there are 33 packages which contain "scp" in their name:
#  python-scp libscscp libscscp-dev liboscpack1 libscscp-doc gap-scscp python3-lesscpy ruby-net-scp liboscpack-dev libscscp1 printer-driver-escpr libroscpp-core-dev libscscp1-dev libnet-scp-perl 
#  python3-scp cl-roscpp-msg python-roscpp-msg golang-github-tmc-scp-dev escputil python-lesscpy libroscpp-msg-dev gap-pkg-scscp libroscpp-serialization0d liblscp6 ros-roscpp-msg bloscpack 
#  oca-addons-hw-escpos libnet-scp-expect-perl libroscpp2d python3-roscpp-msg libroscpp-dev liblscp-dev liblscp-doc 
#  Unable to apply some actions, aborting
 aptitude install tmux # OK

aptitude install man-pages
#   Couldn't find any package matching "man-pages", but there are 3 packages which contain "man-pages" in their description:
#  whichman ebook-speaker mia-doctools 
#   Unable to apply some actions, aborting

#3. Search the internet for 'webmin' and figure out how to install it.
wget http://prdownloads.sourceforge.net/webadmin/webmin_1.960_all.deb
apt-get install perl libnet-ssleay-perl openssl libauthen-pam-perl libpam-runtime libio-pty-perl apt-show-versions python unzip
 dpkg --install webmin_1.960_all.deb
#4. If time permits, search for and install samba including the samba docs pdf files (thousands of pages in two pdf's).
aptitude install samba
