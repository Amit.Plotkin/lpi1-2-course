#!/usr/bin/env bash
############################################
# Owner:Amit Plotkin
# Description: Reverse String
# Version: v1.0.0.0
# Date: 15-09-2020
# Note:
#5.Create a script that will receive file name: if it will be regular file, then print out to its data, if it will be folder then print out files in it.
############################################

FileName=$1 #"Desktop/Bash_Scripts/Looping_new_gear/SomeDir/" 
#echo $FileName
#ls -la

if  [ -f "$FileName" ]; then
    echo "$FileName is a File";

    while read line; do
    # reading each line
    echo $line
    done < $FileName

elif [ -d "$FileName" ]; then
    echo "$FileName is a Directory";

    for file in "$FileName"*
    do
        printf "%s\n" "$file";
    done
    
else
echo "$FileName not Exist";
fi

