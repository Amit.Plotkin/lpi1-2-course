#!/usr/bin/env bash
############################################
# Owner:Amit Plotkin
# Description: 
# Version: v1.0.0.0
# Date: 
############################################

#1. Run a command that displays only your currently logged on user name.
    whoami
#2. Display a list of all logged on users.
    who
#3. Display a list of all logged on users including the command they are running at this very moment.
    w
#4. Display your user name and your unique user identification (userid).
    id
#5. Use su to switch to another user account (unless you are root, you will need the password
#   of the other account). And get back to the previous account.
    su  amit
    exit

#6. Now use su - to switch to another user and notice the difference.
#   Note that su - gets you into the home directory of Tania.
    su - amit
    exit
#7. Try to create a new user account (when using your normal user account). this should fail.
#   (Details on adding user accounts are explained in the next chapter.)
     /usr/sbin/useradd TestUser
#   useradd: Permission denied.
#   useradd: cannot lock /etc/passwd; try again later.

#8. Now try the same, but with sudo before your command
 sudo /usr/sbin/useradd TestUser
#   [sudo] password for amit: 
#   amit is not in the sudoers file.  This incident will be reported.