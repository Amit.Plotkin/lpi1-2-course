#!/usr/bin/env bash
############################################
# Owner:Amit Plotkin 
# Description: Reverse String
# Version: v1.0.0.0
# Date: 15-09-2020
# Note: create script that will take a requiered disk name and will make extended partition type  and will devide it into 8 partitions.
#        - script also should put a filesystem on each of the partisions: ext4 if not defined otherwise.
############################################


source PartitionReposotory.sh

function main()
{
  diskName="$@";
  Create_Extended_Partition "$diskName";
  
}


main "$@"