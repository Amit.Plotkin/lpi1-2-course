#!/usr/bin/env bash
############################################
# Owner:Amit Plotkin 
# Description: 
# Version: v1.0.0.0
# Date:
# Note: 
############################################
##28.13. practice: user management

#1. Create a user account named serena, including a home directory and a description (or
#   comment) that reads Serena Williams. Do all this in one single command.
    useradd -m -d /home/serena -c "Serena Williams" serena
#2. Create a user named venus, including home directory, bash shell, a description that reads
#   Venus Williams all in one single command.
    useradd -m -d /home/venus -c "Venus Williams" -s /bin/bash venus
#3. Verify that both users have correct entries in /etc/passwd, /etc/shadow and /etc/group.
     tail /etc/passwd |grep "Williams"
#serena:x:1001:1001:Serena Williams:/home/serena:/bin/sh
#venus:x:1002:1002:Venus Williams:/home/venus:/bin/bash
 sudo tail /etc/group
#   serena:x:1001:
#   venus:x:1002:
sudo tail /etc/shadow
#serena:!:18560:0:99999:7:::
#venus:!:18560:0:99999:7:::

#4. Verify that their home directory was created.
    sudo ls /home/
    #amit  serena  venus
#5. Create a user named einstime with /bin/date as his default logon shell.
    sudo useradd -m -d /home/einstime -s /bin/date einstime 
#7. What happens when you log on with the einstime user ? Can you think of a useful real
#   world example for changing a user's login shell to an application ?
    su einstime
#   Sun Oct 25 22:38:06 IST 2020
#   
#8. Create a file named welcome.txt and make sure every new user will see this file in their
#   home directory.
    touch /etc/skel/welcome.txt
#9. Verify this setup by creating (and deleting) a test user account.
     useradd -m amit2
     userdel -r amit2
#10. Change the default login shell for the serena user to /bin/bash. Verify before and after
#   you make this change.
 tail /etc/passwd |grep serena
#serena:x:1001:1001:Serena Williams:/home/serena:/bin/sh
usermod -s /bin/bash serena
tail /etc/passwd |grep serena
#serena:x:1001:1001:Serena Williams:/home/serena:/bin/bash
