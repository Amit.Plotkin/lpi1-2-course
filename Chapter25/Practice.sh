#!/usr/bin/env bash
############################################
# Owner:Amit Plotkin 
# Description: Reverse String
# Version: v1.0.0.0
# Date: 20-09-2020
# Note: 
#1. Write a script that receives four parameters, and outputs them in reverse order.
#2. Write a script that receives two parameters (two filenames) and outputs whether those files exist.
#3. Write a script that asks for a filename. Verify existence of the file, then verify that you
#   own the file, and whether it is writable. If not, then make it writable.
#4. Make a configuration file for the previous script. Put a logging switch in the config file,
#   logging means writing detailed output of everything the script does to a log file in /tmp.
############################################

#1 reverce array
params=(a b c) ;#$@
declare -A result
index=0
maxindex=${#params[@]}
for param in ${params}
do
key=$(($maxindex-$index))
$result[$key]=$params;
((index++))
done
echo $result