#!/usr/bin/env bash
############################################
# Owner:Amit Plotkin
# Description: Reverse String
# Version: v1.0.0.0
# Date: 15-09-2020
# Note: collection of methods for partition manipulation
############################################


function Count_Partitions()
{
    driveName="$@"
    # this row has a bug, if the number of partitions is > 0 than the value is adding the row "Device" to count.
    local numberOfPartision=$(fdisk -l $driveName|grep -A100 "Device"|wc -l);
    
   if [[ $numberOfPartision > 0 ]];then
        echo $((numberOfPartision-1));
   else
        echo "0";
   fi
}

#this mehod delete the last partision 
function Delete_Last_Partition(){
   echo "start delete last Partision From $@"
    local diskname="$1";
      
    fdisk "$diskname" << EOF
d

w
EOF

}

function Delete_All_Partitions()
{
    echo "start delete All Partision From $@"
   local diskName="$@"
   local numberOfPartision=$(Count_Partitions "$diskName")
   echo "number of partitions is $numberOfPartision"
   i="0"

    while [ $i -lt $numberOfPartision ]
    do
        Delete_Last_Partition "$diskName"
        i=$[$i+1]
    done

}



# this method get 2 params 1)disk path 2) partision size and creates partision of that size on the selected disk
function Create_Partition()
{
    local diskname="$1";
    local Size=+"$2"
fdisk "$diskname" <<EOF
n
p


${Size}
w
EOF
}

function Create_Extended_Partition()
{
    local diskname="$1";
    local Size=+"$2"
fdisk "$diskname" <<EOF
n
e


${Size}
w
EOF
}

function HelpMe()
{
    echo "this is the manual documentation ...";
}

function Create_File_System(){
   diskName="$1"
   FSType="$2"

   case  "$FSType" in
       bfs)
        mkfs.bfs "$diskName"
       ;;
       cramfs)
        mkfs.cramfs "$diskName"
       ;;
       exfat)
        mkfs.ext2 "$diskName"
       ;;
       ext2)
        mkfs.ext2 "$diskName"
       ;;
       ext3)
        mkfs.ext3 "$diskName"
       ;;       
       ext4)
        mkfs.ext4 "$diskName"
       ;;
       fat)
        mkfs.fat "$diskName"
       ;;
       minix)
        mkfs.minix "$diskName"
       ;;
       msdos)
        mkfs.msdos "$diskName"
       ;;
       ntfs)
        mkfs.ntfs "$diskName"
       ;;
       vfat)
        mkfs.vfat "$diskName"
       ;;
esac
}

function Mound_Disk_On_Dest(){
    mountDrive="$1";
    mountFolder="$2"
    mount "$mountDrive" "$mountFolder"
}

function Get_Disk_Partisions(){
    local diskName="$@";
    local declare -a PartitionArr;
    local result=$(fdisk -l "$diskName" |awk '{print $1}' |grep -A100 /dev |tr '\n' ' ')

    echo $result; # skip the first word "Device "
}

echo "this is the manual documentation"


