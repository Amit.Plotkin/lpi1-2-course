#!/usr/bin/env bash
############################################
# Owner:Amit Plotkin
# Description: Reverse String
# Version: v1.0.0.0
# Date: 15-09-2020
# Note:
#3.Two parter:
#   a. Write a script that will request 2 parameters(read), time and command. the  script should notify that command should be executed at notified time (should work as clock alarm).
#   b. Please verify that you are recieving 2 paramenters
############################################

TimeToExec=$(date -d '5 seconds' +'%s')
echo $TimeToExec
MyCommand="echo helloWorld"

#Read input
#read -p "add time to Execute: " TimeToExec
#read -p "add comand to execute: " MyCommand

while [ 0 ] #[ "$date" <= "$TimeToExec" ]
do
    timeNow=$(date +"%s")
    
    if  [ "$timeNow" -lt "$TimeToExec" ]
    then
        sleep 2;
        echo $timeNow;
    else
        break;
    fi
    
done

#eval: eval [arg ...]
#    Execute arguments as a shell command.
eval $MyCommand


