#!/usr/bin/env bash
############################################
# Owner:Amit Plotkin 
# Description: Reverse String
# Version: v1.0.0.0
# Date: 15-09-2020
# Note: seems that it works for EX1 and EX2
# 1.Write a script that will receive($1) your name and will print it in reverse.
# 2.Write a script that will receive long sentance and will print it in reverse.
############################################

#read input from the console
read -p "Please enter String:" InputData

#get the length of the  string
length=$((${#InputData}-1));

#loop the index and print the length-i char
for i in $( seq 0 $length )
do
        #take one char at index length-i
       printf "${InputData:$(($length-$i)):1}"
done
printf "\n"

