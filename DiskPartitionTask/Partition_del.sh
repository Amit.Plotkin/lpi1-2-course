#!/usr/bin/env bash
set -x
############################################
# Owner:Amit Plotkin 
# Description: Reverse String
# Version: v1.0.0.0
# Date: 15-09-2020
# Note:  create script that will take a requiered disk name and will delete all current partitions partitions.
############################################

source PartitionReposotory.sh
clear;
function main()
{
    local disk_name="$@"
    if [[ "$disk_name" == "Help" ]]; then

       echo "sdfsdfsd"
        exit 0;
    fi

    #if disk name not empty string and disk exists
    if  [[ -n $disk_name ]] && [[ -b $disk_name  ]];then

        local disk_partitions="";

         disk_partitions="$(Get_Disk_Partisions $disk_name)"; # disk_partitions is an array of partition
           echo "***************************"
           echo "${disk_partitions}"
           echo "${disk_partitions[@]}"
            echo "***************************"
        for Partition in ${disk_partitions[@]}; do
            echo "Amit Del $Partition"
            Delete_Partition "$Partision";
            done
        echo "partisions Deleted Successfully";
        exit 0;
    else
        echo "$disk_name is incorrect or missing";
        HelpMe;
        exit 1;
    fi

}


main "$@"